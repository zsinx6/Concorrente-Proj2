#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <math.h>
#define NTHREADS 1024

struct timeval tv1, tv2;

using namespace cv;

//faz a alocacao da matriz de forma linear
uchar **alloc_2d(int rows, int cols) {
	uchar *data = (uchar *)malloc(rows*cols*sizeof(uchar));
	uchar **array= (uchar **)malloc(rows*sizeof(uchar*));
	for (int i=0; i<rows; i++)
		array[i] = &(data[cols*i]);

	return array;
}
uchar **alloc_2dcuda(int rows, int cols) {
	uchar *data;
	uchar **array;
	cudaMalloc(&data, rows*cols*sizeof(uchar));
	cudaMalloc(&array, rows*sizeof(uchar*));
	for (int i=0; i<rows; i++)
		array[i] = &(data[cols*i]);

	return array;
}

__global__ void calcula_soma(uchar **matriz, int row, int col){
	int i, j, a, b, soma, c;
	float media;
	int inicioi, fimi, inicioj, fimj, idi, idj;
	int divalt, divlarg;
	int id = threadIdx.x;

	idi = id;
	idj = id/4;
	divalt = row/32; //divide a altura
	divlarg = col/32; //divide a largura

	//faz a divisao de tarefas entre as threads
	inicioi = (idi*divalt)%16;
	fimi = (idi*divalt)%16 + divalt;
	if(row > inicioi+divalt)
		fimi = row;

	inicioj = (idj*divlarg)%16;
	if(col > inicioj+divlarg)
		fimj = col;

	fimj = (idj*divlarg)%16 + divlarg;

	//repete o algoritmo do sequencial da mesma forma, porem cada thread executa sua parte
	for(i=inicioi;i<fimi;i++){
		for(j=inicioj;j<fimj;j++){
			soma = 0;
			c = 0;
			if(i==0 && j==0){
				for(a=0;a<3;a++){
					for(b=0;b<3;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==1 && j==0){
				for(a=0;a<4;a++){
					for(b=0;b<3;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==0 && j==1){
				for(a=0;a<3;a++){
					for(b=0;b<4;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==1 && j==1){
				for(a=0;a<4;a++){
					for(b=0;b<4;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==row-1 && j==col-1){
				for(a=row-3;a<row;a++){
					for(b=col-3;b<col;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==row-2 && j==col-1){
				for(a=row-4;a<row;a++){
					for(b=col-3;b<col;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==row-1 && j==col-2){
				for(a=row-3;a<row;a++){
					for(b=col-4;b<col;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==row-2 && j==col-2){
				for(a=row-4;a<row;a++){
					for(b=col-4;b<col;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==row-1 && j==0){
				for(a=row-3;a<row;a++){
					for(b=0;b<3;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==row-2 && j==0){
				for(a=row-4;a<row;a++){
					for(b=0;b<3;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==row-1 && j==1){
				for(a=row-3;a<row;a++){
					for(b=0;b<4;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==row-2 && j==1){
				for(a=row-4;a<row;a++){
					for(b=0;b<4;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==0 && j==col-1){
				for(a=0;a<3;a++){
					for(b=col-3;b<col;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==0 && j==col-2){
				for(a=0;a<3;a++){
					for(b=col-4;b<col;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==1 && j==col-1){
				for(a=0;a<4;a++){
					for(b=col-3;b<col;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==1 && j==col-2){
				for(a=0;a<4;a++){
					for(b=col-4;b<col;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==0 && (j>=2 && j<=col-3)){
				for(a=0;a<3;a++){
					for(b=j-2;b<j+3;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if((i>=2 && i<=row-3) && j==0){
				for(a=i-2;a<i+3;a++){
					for(b=0;b<3;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if((i>=2 && i<=row-3) && j==col-1){
				for(a=i-2;a<i+3;a++){
					for(b=j-2;b<j+1;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==row-1 && (j>=2 && j<=col-3)){
				for(a=i-2;a<i+1;a++){
					for(b=j-2;b<j+3;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==1 && (j>=2 && j<=col-3)){
				for(a=0;a<4;a++){
					for(b=j-2;b<j+3;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if((i>=2 && i<=row-3) && j==1){
				for(a=i-2;a<i+3;a++){
					for(b=0;b<4;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if((i>=2 && i<=row-3) && j==col-2){
				for(a=i-2;a<i+3;a++){
					for(b=j-2;b<j+2;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else if(i==row-2 && (j>=2 && j<=col-3)){
				for(a=i-2;a<i+2;a++){
					for(b=j-2;b<j+3;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			else{
				for(a=i-2;a<i+3;a++){
					for(b=j-2;b<j+3;b++){
						soma = soma + matriz[a][b];
						c++;
					}
				}
			}
			if(c != 0){
				media = (float) soma/c;
				matriz[i][j] = (uchar) media;
			}
		}
	}
	//return matriz;
}

int main( int argc, char** argv ){
	Mat image, dst;

	//inicia a comunicacao com os nos e limpa o argc e o argv dos argumentos do mpi

	//inicia a quantidade de nos
	//pega o id do no atual em taskid
	if(argc != 3){
		printf("Error\n");
		return -1;
	}

	int color = atoi(argv[2])-1;
	if(strcmp(argv[2], "1") == 0)
		image = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
	else if(strcmp(argv[2], "2") == 0)
		image = imread(argv[1], CV_LOAD_IMAGE_COLOR);
	int ch = image.channels();
	int r = image.rows;
	int c = image.cols;
	int i, j, k;
	uchar** mat1 = NULL;
	uchar** mat2 = NULL;
	uchar** mat3 = NULL;
	uchar** mat1cu = NULL;
	uchar** mat2cu = NULL;
	uchar** mat3cu = NULL;

	//faz alocacao das matrizes
	if(!color){
		mat1 = alloc_2d(r,c);
		mat1cu = alloc_2dcuda(r,c);
	}
	else{
		mat1 = alloc_2d(r,c);
		mat2 = alloc_2d(r,c);
		mat3 = alloc_2d(r,c);
		mat1cu = alloc_2dcuda(r,c);
		mat2cu = alloc_2dcuda(r,c);
		mat3cu = alloc_2dcuda(r,c);
	}

	//taskid 0 eh o id do master
	gettimeofday(&tv1, NULL);

	//pega as informacoes da imagem
	if(!color){
		k=0;
		for(i=0; i<r; i++){
			for (j=0; j<c; j++){
				mat1[i][j] = image.data[k++];
			}
		}
		cudaMemcpy(mat1cu,mat1,r*c*sizeof(uchar),cudaMemcpyHostToDevice);
	}
	else{
		for(i=0; i<r; i++){
			for (j=0; j<c; j++){
				mat1[i][j] = image.at<Vec3b>(i,j)[0];
				mat2[i][j] = image.at<Vec3b>(i,j)[1];
				mat3[i][j] = image.at<Vec3b>(i,j)[2];
			}
		}
		cudaMemcpy(mat1cu,mat1,r*c*sizeof(uchar),cudaMemcpyHostToDevice);
		cudaMemcpy(mat2cu,mat2,r*c*sizeof(uchar),cudaMemcpyHostToDevice);
		cudaMemcpy(mat3cu,mat3,r*c*sizeof(uchar),cudaMemcpyHostToDevice);

		calcula_soma<<<1, NTHREADS>>>(mat1cu, r, c);
		if(color){
		calcula_soma<<<1, NTHREADS>>>(mat2cu, r, c);
		calcula_soma<<<1, NTHREADS>>>(mat3cu, r, c);
		}


		if(!color){
			cudaMemcpy(mat1,mat1cu,r*c*sizeof(uchar),cudaMemcpyDeviceToHost);
			k=0;
			for(i=0; i<r; i++){
				for(j=0; j<c; j++){
					image.data[k++] = mat1[i][j];
				}
			}
		}
		else{
			cudaMemcpy(mat1,mat1cu,r*c*sizeof(uchar),cudaMemcpyDeviceToHost);
			cudaMemcpy(mat2,mat2cu,r*c*sizeof(uchar),cudaMemcpyDeviceToHost);
			cudaMemcpy(mat3,mat3cu,r*c*sizeof(uchar),cudaMemcpyDeviceToHost);
			for(i=0; i<r; i++){
				for(j=0; j<c; j++){
					image.at<Vec3b>(i,j)[0] = mat1[i][j];
					image.at<Vec3b>(i,j)[1] = mat2[i][j];
					image.at<Vec3b>(i,j)[2] = mat3[i][j];
				}
			}
		}

		//finalizada a escrita da image, escrever no disco:
		gettimeofday(&tv2, NULL);
		printf ("Total time = %f seconds\n",
				(double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
				(double) (tv2.tv_sec - tv1.tv_sec));
		imwrite(argv[1], image);
		//desalocacao de memoria
		if(mat1 != NULL){
			free(mat1[0]);
			free(mat1);
		}
		if(mat2 != NULL){
			free(mat2[0]);
			free(mat2);
		}
		if(mat3 != NULL){
			free(mat3[0]);
			free(mat3);
		}
	}
	return 0;
}

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

struct timeval tv1, tv2;

using namespace cv;

//funcao usada para alocacao linear de matrizes
uchar **alloc_2d(int rows, int cols) {
    uchar *data = (uchar *)malloc(rows*cols*sizeof(uchar));
    uchar **array= (uchar **)malloc(rows*sizeof(uchar*));
    for (int i=0; i<rows; i++)
        array[i] = &(data[cols*i]);

    return array;
}

//funcao que faz o smooth para uma matriz row x col
uchar** calcula_soma(uchar **matriz, int row, int col){
    int i, j, a, b, soma, c;
    float media;

    for(i=0;i<row;i++){
        for(j=0;j<col;j++){
            soma = 0;
            c = 0;
            //todas as condicoes de borda sao tratadas, transformando o quadrado 5x5 em um quadrado menor quando necessario
            if(i==0 && j==0){
                for(a=0;a<3;a++){
                    for(b=0;b<3;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if(i==1 && j==0){
                for(a=0;a<4;a++){
                    for(b=0;b<3;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else if(i==0 && j==1){
                for(a=0;a<3;a++){
                    for(b=0;b<4;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else if(i==1 && j==1){
                for(a=0;a<4;a++){
                    for(b=0;b<4;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if(i==row-1 && j==col-1){
                for(a=row-3;a<row;a++){
                    for(b=col-3;b<col;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else if(i==row-2 && j==col-1){
                for(a=row-4;a<row;a++){
                    for(b=col-3;b<col;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else if(i==row-1 && j==col-2){
                for(a=row-3;a<row;a++){
                    for(b=col-4;b<col;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if(i==row-2 && j==col-2){
                for(a=row-4;a<row;a++){
                    for(b=col-4;b<col;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if(i==row-1 && j==0){
                for(a=row-3;a<row;a++){
                    for(b=0;b<3;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else if(i==row-2 && j==0){
                for(a=row-4;a<row;a++){
                    for(b=0;b<3;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else if(i==row-1 && j==1){
                for(a=row-3;a<row;a++){
                    for(b=0;b<4;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else if(i==row-2 && j==1){
                for(a=row-4;a<row;a++){
                    for(b=0;b<4;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if(i==0 && j==col-1){
                for(a=0;a<3;a++){
                    for(b=col-3;b<col;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else if(i==0 && j==col-2){
                for(a=0;a<3;a++){
                    for(b=col-4;b<col;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else if(i==1 && j==col-1){
                for(a=0;a<4;a++){
                    for(b=col-3;b<col;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else if(i==1 && j==col-2){
                for(a=0;a<4;a++){
                    for(b=col-4;b<col;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if(i==0 && (j>=2 && j<=col-3)){
                for(a=0;a<3;a++){
                    for(b=j-2;b<j+3;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if((i>=2 && i<=row-3) && j==0){
                for(a=i-2;a<i+3;a++){
                    for(b=0;b<3;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if((i>=2 && i<=row-3) && j==col-1){
                for(a=i-2;a<i+3;a++){
                    for(b=j-2;b<j+1;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if(i==row-1 && (j>=2 && j<=col-3)){
                for(a=i-2;a<i+1;a++){
                    for(b=j-2;b<j+3;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else if(i==1 && (j>=2 && j<=col-3)){
                for(a=0;a<4;a++){
                    for(b=j-2;b<j+3;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if((i>=2 && i<=row-3) && j==1){
                for(a=i-2;a<i+3;a++){
                    for(b=0;b<4;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if((i>=2 && i<=row-3) && j==col-2){
                for(a=i-2;a<i+3;a++){
                    for(b=j-2;b<j+2;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }

            else if(i==row-2 && (j>=2 && j<=col-3)){
                for(a=i-2;a<i+2;a++){
                    for(b=j-2;b<j+3;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            else{
                for(a=i-2;a<i+3;a++){
                    for(b=j-2;b<j+3;b++){
                        soma = soma + matriz[a][b];
                        c++;
                    }
                }
            }
            if(c != 0){
                media = (float) soma/c;
                //eh retirada a media dos pixels e guardada na matriz da imagem
                matriz[i][j] = (uchar) media;
            }
        }
    }
    return matriz;
}

int main( int argc, char** argv ){
    Mat image, dst;

    if(argc != 3){
        printf("Usage: './binary path/to/image 1/2' (grayscale/rgb)\n");
        return -1;
    }

    int color = atoi(argv[2])-1;

    //o segundo argumento da funcao eh o tipo da imagem, se 1 eh escala de cinza, se 2 eh rgb
    if(strcmp(argv[2], "1") == 0)
        image = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
    else if(strcmp(argv[2], "2") == 0)
        image = imread(argv[1], CV_LOAD_IMAGE_COLOR);

    //apos a imagem carregada, pega-se as dimensoes da mesma e os canais de cores
    int ch = image.channels();
    int r = image.rows;
    int c = image.cols;
    int i,j,k;
    uchar** mat1 = NULL;
    uchar** mat2 = NULL;
    uchar** mat3 = NULL;

    //alocacao das matrizes
    if(!color){
        mat1 = alloc_2d(r,c);
    }
    else{
        mat1 = alloc_2d(r,c);
        mat2 = alloc_2d(r,c);
        mat3 = alloc_2d(r,c);
    }

    gettimeofday(&tv1, NULL);

    //se a imagem for colorida pega-se as informacoes das 3 cores
    //caso contrario apenas a escala de cinza
    if(!color){
        k=0;
        for(i=0; i<r; i++){
            for (j=0; j<c; j++){
                mat1[i][j] = image.data[k++];
            }
        }
    }
    else{
        for(i=0; i<r; i++){
            for (j=0; j<c; j++){
                mat1[i][j] = image.at<Vec3b>(i,j)[0];
                mat2[i][j] = image.at<Vec3b>(i,j)[1];
                mat3[i][j] = image.at<Vec3b>(i,j)[2];
            }
        }
    }
    //chama a funcao passando as matrizes e recebe de volta as mesmas alteradas
    mat1 = calcula_soma(mat1, r, c);
    if(color){
        mat2 = calcula_soma(mat2, r, c);
        mat3 = calcula_soma(mat3, r, c);
    }

    //entao deve-se alterar a imagem atual para a nova
    if(!color){
        k=0;
        for(i=0; i<r; i++){
            for(j=0; j<c; j++){
                image.data[k++] = mat1[i][j];
            }
        }
    }
    else{
        for(i=0; i<r; i++){
            for(j=0; j<c; j++){
                image.at<Vec3b>(i,j)[0] = mat1[i][j];
                image.at<Vec3b>(i,j)[1] = mat2[i][j];
                image.at<Vec3b>(i,j)[2] = mat3[i][j];
            }
        }
    }

    gettimeofday(&tv2, NULL);
    printf ("Total time = %f seconds\n",
            (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
            (double) (tv2.tv_sec - tv1.tv_sec));
    //finalizada a escrita da image, escrever no disco:
    imwrite(argv[1], image);

    //desalocacao de memoria
    if(mat1 != NULL){
        free(mat1[0]);
        free(mat1);
    }
    if(mat2 != NULL){
        free(mat2[0]);
        free(mat2);
    }
    if(mat3 != NULL){
        free(mat3[0]);
        free(mat3);
    }
    return 0;
}
